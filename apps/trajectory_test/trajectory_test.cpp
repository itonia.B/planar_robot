#include "trajectory_generation.h"
#include <iostream>
using namespace std;



int main(){
    //Declaring the variables I need 
    double Dt=2; //time interval
    double dt=pow(10,-6); //step
    Eigen::Vector2d X_i = {0,0}; //initial position
    Eigen::Vector2d X_f = {1,1}; //final position
    Eigen::Vector2d cart_position; //initialize a vector for the position
    Eigen::Vector2d cart_velocity; //initialize a vector for the speed
    Eigen::Vector2d num_velocity; ////initialize a vector for the numarical speed 

    //start time
    double t=0;
    //I define my function "my trajectory" that initializes the polinomial parameters for xi, xf and Dt given
    Point2Point mytrajectory=Point2Point(X_i,X_f,Dt);

    // Compute the trajectory for given initial and final positions.

    //I create a loop that runs during Dt with a dt step.
    //At each iteration calculates the position and the Cartesian speed
    while(t<Dt){
        cart_position=mytrajectory.X(t);
        cart_velocity=mytrajectory.dX(t);
        // Check numerically the velocities given by the library 
        //I apply the formula of the numerical derivative :(current_position - previous_position)/step dXn(t)=(X(t)-X(t_dt))/dt ()
        //I compared the two speeds and they are almost identical
        num_velocity=(mytrajectory.X(t)-mytrajectory.X(t-dt))/dt;
        //Display some of the computed points
        cout<<"\n"<<"x:"<<cart_position(0)<<"   "<<"y:"<<cart_position(1)<<"   "<<"vx:"<<cart_velocity(0)<<"   "<<"vx_num:"<<num_velocity(0)<<"   "<<"vy:"<<cart_velocity(1)<<"   "<<"vy_num:"<<num_velocity(1)<<endl;
        t=t+dt;
    }

    // Check initial and final velocities
    //verification: they are equal to zero
    cout<<"\n initial velocity \n" ;
    cout<< mytrajectory.dX(0) << "\n";
    
    cout<< "\n final velocity \n" ;
    cout<< mytrajectory.dX(Dt) << "\n";
}


