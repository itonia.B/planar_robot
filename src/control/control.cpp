#include "control.h"
#include <iostream>
Controller::Controller(RobotModel &rmIn):
  model  (rmIn) 
{
}

Eigen::Vector2d Controller::Dqd ( 
          const Eigen::Vector2d & q,
          const Eigen::Vector2d & xd,
          const Eigen::Vector2d & Dxd_ff
                      )
{  
  //TODO Compute joint velocities able to track the desired cartesian position
   
  //(pas besoin de faire mais c'était ma 1ère idée (quelque chose que je n'ai pas bien assimilé)
   //RobotModel mykinematic = RobotModel(l1,l2);
   //mykinematic.FwdKin(X,J,q);) note personnelle

  //forward kinematic model
  model.FwdKin(X,J,q);
  //computing of the error betwen the disered position et the current position
  Eigen::Vector2d error = xd-X;
  //computing of the desired cartesian velocity where kp is a gain and Dxd_ff obtained using the trajectory generation
  dX_desired = kp*error +  Dxd_ff;
  // Inverse differential kinematic model dq=J⁻¹(q).dX
  Eigen::Vector2d dq_desired = J.inverse()*dX_desired;
  return dq_desired;
}


