#include "trajectory_generation.h"
#include <iostream>

Polynomial::Polynomial(){};

Polynomial::Polynomial(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO initialize the object polynomial coefficients
  
  //I use the private variables declared in trajectory_generation.h
  //I assign a value to all parameters
  pi=piIn;
  pf=pfIn;
  Dt=DtIn;
  a[0]=pi;
  a[1]=0;
  a[2]=0;
  a[3]=10*(pf-pi);
  a[4]=-15*(pf-pi);
  a[5]=6*(pf-pi);
};

void          Polynomial::update(const double &piIn, const double &pfIn, const double & DtIn){
  //TODO update polynomial coefficients

  //since this function will be in a robot control loop, 
  //I need to update the parameters each time the position changes to calculate the new trajectory
  //to allow me to go to pf_desired from the current position (which changes)
  pi=piIn;
  pf=pfIn;
  Dt=DtIn;
  a[0]=pi;
  a[1]=0;
  a[2]=0;
  a[3]=10*(pf-pi);
  a[4]=-15*(pf-pi);
  a[5]=6*(pf-pi);
  std::cout << "Polynomial Updated !\n";
};

const double  Polynomial::p     (const double &t){
  //TODO compute position

  //computing of the position from the trajectory generation polinomial 
  double p = a[0] + a[1]*(t/Dt) + a[2]*pow((t/Dt),2) + a[3]*pow((t/Dt),3) + a[4]*pow((t/Dt),4) + a[5]*pow((t/Dt),5);
  return p;
};

const double  Polynomial::dp    (const double &t){
  //TODO compute velocity

  //computing of velocity with the trajectory generation polinomial derivative
  double dp = a[1]/Dt + 2*a[2]*t/pow(Dt,2) + 3*a[3]*pow(t,2)/pow(Dt,3) + 4*a[4]*pow(t,3)/pow(Dt,4) + 5*a[5]*pow(t,4)/pow(Dt,5);
  return dp;
};

Point2Point::Point2Point(const Eigen::Vector2d & xi, const Eigen::Vector2d & xf, const double & DtIn){
  //TODO initialize object and polynomials
  Eigen::Vector2d X_i=xi;
  Eigen::Vector2d X_f=xf;
  Dt=DtIn;
  
  std::cout << "Buiding X polynomial";
  polx = Polynomial(X_i(0),X_f(0),Dt);
  std::cout << "Buiding Y polynomial";
  poly = Polynomial(X_i(1),X_f(1),Dt);
  //computing of the polinomial's coefficients knowing initial position, final position and time interval between them
  //I display things to check the code step by step. it was my first function.
}

Eigen::Vector2d Point2Point::X(const double & time){
  //TODO compute cartesian position
  Dt=time;
  Eigen::Vector2d X(polx.p(Dt),poly.p(Dt));
  return X;
  //computing of the cartesian position (x,y) 
}

Eigen::Vector2d Point2Point::dX(const double & time){
  //TODO compute cartesian velocity
  Dt=time;
  Eigen::Vector2d dX(polx.dp(Dt),poly.dp(Dt));
  return dX;
  //computing of the cartesian velocity(dx,dy)
}