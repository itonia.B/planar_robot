#include "control.h"
#include "trajectory_generation.h"
#include <iostream>

using namespace std;

int main(){
  // Show using all three libraries, simulate the motion of a planar robot
  // For a initial angle_position  q= M_PI_2, M_PI_4 and l1  = 0.4, l2 = 0.5
  // simulate a motion achieving Xf  = 0.0, 0.6

  //update the arm's length and time interval
  const double l1=0.4;
  const double l2=0.5;
  double Dt=2;

  //initialize the joint and cartesian positions and the time step
  Eigen::Vector2d q{M_PI_2, M_PI_4};
  double dt=pow(10,-3);
  Eigen::Vector2d X_i = {0.6,0.0};
  Eigen::Vector2d X_f = {0.0,0.6};

  //declare the vectors I will need
  Eigen::Vector2d X; //current cartesian position
  Eigen::Vector2d Xd; //desired cartesian position
  Eigen::Vector2d Dxd_ff; //desired position obtained using the trajectory generation
  Eigen::Vector2d dq_desired; //desired joint position
  Eigen::Matrix2d J; //the Jacobian matrix

  double t=0;//start time
  RobotModel mykinematic=RobotModel(l1,l2) ;
  mykinematic.FwdKin(X,J,q); //forward kinematic model
  Point2Point mytrajectory=Point2Point(X_i,X_f,Dt); //trajectory generation
  Controller mycrontroller(mykinematic); //initialize the control part of the RobotModel

  //control loop:
  //At each iteration it receives the articuar position, then calculates X and J using the direct geometric model. 
  //Then it compares it to the desired position, corrects the value and calculates the desired velocity. 
  //then calculates the desired joint velocity using the inverse kinematic model. if we didn’t get to Xd, we update q and start again. 
  while(t<Dt){
    Xd=mytrajectory.X(t); //computing the desired cartesian position
    Dxd_ff=mytrajectory.dX(t); //computing the desired cartesian position with the polynomial of trajectory generation
    dq_desired=mycrontroller.Dqd(q,Xd,Dxd_ff); // computing of the desired joint velocity
    q=q+dq_desired*dt; //update the current joint position
    mykinematic.FwdKin(X,J,q); //computing current position and the jacobian matrix
    cout<<"\n"<<"t="<<t<<"\n"<<"current_position="<<X<<"\n"<<"desired_position="<<Xd<<endl;
    t=t+dt;
  }

}