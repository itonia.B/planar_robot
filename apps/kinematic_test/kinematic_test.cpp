#include "kinematic_model.h"
#include <iostream>

using namespace std;

int main(){
  //update the arms's length of the robot 2 Dofs with chosen values
  const double l1=1; 
  const double l2=2;
  //declaration of the vector that will contain the cartesian poistion(x,y) and the matrix that will contain the Jacobian
  //outputs of the kinematic function
  Eigen::Vector2d xOut;
  Eigen::Matrix2d JOut;
  //initial joint position
  Eigen::Vector2d qIn{M_PI/3.0,M_PI_4};
  //small variation of the joint position
  Eigen::Vector2d dqIn{0.05,0.05};
  //outputs of the kinematic function when q=qIn+dq   
  Eigen::Vector2d xOut2;
  Eigen::Matrix2d JOut2;
  //declaration of the vectors that will contain the cartesian velociticy for qIn et qIn+dq 
  Eigen::Vector2d dxOut1;
  Eigen::Vector2d dxOut2;


  // Compute the forward kinematics and jacobian matrix for q =  M_PI/3.0,  M_PI_4

  RobotModel mykinematic=RobotModel(l1,l2);
  //forward kinematic model with qIn
  mykinematic.FwdKin(xOut,JOut,qIn);
  cout<<"\n"<<"x_position="<<xOut(0)<<"   "<<"y_position="<<xOut(1)<<"\n"<<"\n"<<"J="<<JOut<<"\n"<<"\n"<<"θ1="<<qIn(0)<<"   "<<"θ2="<<qIn(1)<<endl;
  
  // For a small variation of q, compute the variation on X and check dx = J . dq 

  //forward kinematic model with qIn+dq 
  mykinematic.FwdKin(xOut2,JOut2,(qIn+dqIn));
  //computing of dX by subtracting between the first cartesian position obtained for qIn and the second for when q=qIn+dqIn
  dxOut1=(xOut2-xOut);
  //computing of dX using the differential kinematic model
  dxOut2=JOut*dqIn;

  //comparison of found values of dX. 
  //I notice that if I apply a small variation dqIn:
  // dX = J.dq = (the cartesian position of qIn) - (the cartesian position of qIn+dq)
  cout<<"\n"<<"dx1="<<dxOut1<<"\n"<<"\n"<<"dx2=J*dq="<<dxOut2<<endl;
}


